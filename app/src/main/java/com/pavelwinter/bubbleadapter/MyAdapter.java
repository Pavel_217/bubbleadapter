package com.pavelwinter.bubbleadapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by newuser on 13.09.2017.
 */

public class MyAdapter extends BaseAdapter {
    Context ctx;
    LayoutInflater lInflater;
    ArrayList<Mock> objects;

    MyAdapter(Context context, ArrayList<Mock> mocks) {
        ctx = context;
        objects = mocks;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    // кол-во элементов
    @Override
    public int getCount() {
        return objects.size();
    }

    // элемент по позиции
    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    // id по позиции
    @Override
    public long getItemId(int position) {
        return position;
    }

    // пункт списка
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // используем созданные, но не используемые view
        View view = convertView;

        Mock mock = getMock(position);

       // if (view == null) {

            if (mock.getFrom()){

            view = lInflater.inflate(R.layout.my_message, parent, false);
       }else {view = lInflater.inflate(R.layout.that_guy_message, parent, false);
       }


        //Mock mock = getMock(position);

        // и картинка
        ((TextView) view.findViewById(R.id.tv_title)).setText(mock.getName());
        ((TextView) view.findViewById(R.id.tv_descr)).setText(mock.getDescription() + "");

        return view;
    }

    // товар по позиции
    Mock getMock(int position) {
        return ((Mock) getItem(position));
    }

}