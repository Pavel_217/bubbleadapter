package com.pavelwinter.bubbleadapter;

/**
 * Created by newuser on 13.09.2017.
 */

public class Mock {
    public Mock(String name, String description, boolean from) {
        this.name = name;
        this.description = description;
        this.from = from;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean getFrom() {
        return from;
    }

    public void setFrom(boolean from) {
        this.from = from;
    }

    private String name;
    private String description;

    //true is mine,and false otherwise
    private boolean from;

}
