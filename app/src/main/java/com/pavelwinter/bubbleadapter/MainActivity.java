package com.pavelwinter.bubbleadapter;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {


    ArrayList<Mock> products = new ArrayList<Mock>();
    MyAdapter myAdapter;


    public static boolean getRandomBoolean() {
        return Math.random() < 0.5;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        // создаем адаптер
        fillData();
        myAdapter = new MyAdapter(this, products);

        // настраиваем список
        ListView lvMain = (ListView) findViewById(R.id.lvMain);
        lvMain.setAdapter(myAdapter);
    }

    // генерируем данные для адаптера
    void fillData() {
        for (int i = 1; i <= 20; i++) {
            products.add(new Mock("Title"+i,"description description"+1,getRandomBoolean()));
        }
    }
}
